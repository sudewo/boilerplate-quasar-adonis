
### Setup Quasar as Frontend Development
```bash
npx quasar create buatin-boilerplate && cd buatin-boilerplate
```

- `quasar dev`  //run development
- `yarn run lint` //lint files
- `quasar build` //build app for production
- `configure files` https://quasar.dev/quasar-cli/quasar-conf-js


### Setup Adonis as API
```bash
npx adonis new api --api-only
```
```
  cd api
  adonis serve --dev
 ```

 ### Setup Development dengan satu terminal
 - update sricpts in package.json
```json
"scripts": {
    "q:spa": "quasar dev -m spa",
    "api": "npx nodemon --watch api api/server.js",
    "dev": "npx concurrently \"yarn api\" \"yarn q:spa\"",
}
```
- run development quasar and adonis in single terminal
`yarn dev`
- open `http://localhost:8080` quasar
- open `http://localhost:3333` adonis api

### Setup adonis Graphql api
1. Install graphql package
    ```bash
    cd api
    adonis install adonis-graphql
    ```

2. Registering Adonis Graphql Provider inside `start/app.js`
    ```js
    const providers = [
      'adonis-graphql/providers/GraphQLProvider'
    ]
    ```

3. Add Graphql route and Graphiql for user interface
    ```js
    Route.post('/graphql', context => {
      return GraphQLServer.handle(context)
    })

    Route.get('/graphiql', context => {
      return GraphQLServer.handleUI(context)
    })
    ```
4. if found error eslint `use` keyword, update global `.eslintrc.js`
    ```js
    globals: {
        'use': true
      },
    ```

5. create schema
    ```js
    # app/Schema/Hello.graphql
    type Query {
      hello: String
    }
    ```
6. create resolvers
    ```js
    // app/Resolvers/Hello.js
    module.exports = {
      Query: {
        hello: () => 'World'
      }
    }
    ```
7. update graphql config at `config/graphql.js`
    ```js
    'use strict'

    const { join } = require('path')

    module.exports = {
      options: {
        debug: false,
        endpointURL: '/graphql'
      },

      schema: join(__dirname, '../app/Schema'),
      resolvers: join(__dirname, '../app/Resolvers')
    }
    ```
8. re-run application test graphql at `http://localhost:3333/graphiql`
    ```graphql
    {
      hello
    }
    ```

    DONE, Now we have boilerplate for develop app with graphql support :)

## enable default support
  - cors allow origin
  - default support for email using mailtrap
  - add custom response for api versioning
  - enable static files for email templates
  - add default `public/uploads` folder

### Setup Github

- create gitlab repository
  ```bash
  git init && git remote add origin git@gitlab.com:sudewo/buatin-boilerplate.git && git add . && git commit -am "Initial Commit" && git push -u origin master
  ```
