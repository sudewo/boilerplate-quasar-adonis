# Boilerplate quasar adonis

Boilerplate untuk membuat aplikasi dengan quasar sebagai frontend tools dan adonis sebagai api.

why adonis+quasar:
 - why adonis : Complete package to build api with route, model, db, migration feature, etc.
 - why quasar : built mobile, desktop and web with single codebase, with hot reload support.
 - why create this : I want  to develop app in single repo, to make development api and frontend is easy to manage, then use single command to running frontend and api at the same time with hot reload support, without split terminal to run development and split repo between api and frontend.

## enable default support
  - for adonis, I create boilerplate using `--api-only` then enable some feature :
    - cors allow origin
    - default support for email using mailtrap
    - add custom response for api versioning
    - enable static files for email templates
    - add default `public/uploads` folder

### how to use
  ```bash
  1. > git clone git@gitlab.com:sudewo/boilerplate-quasar-adonis.git boilerplate-quasar-adonis && cd boilerplate-quasar-adonis && rm -rf .git && yarn && cd api && yarn && cp .env.example .env && cd .. && code . && yarn dev
  2. > adonis key:generate
  ```
