'use strict'

class ResponseWithVersion {
  async handle (ctx, next) {
    await next()
    const existingResponse = ctx.response._lazyBody.content
    const Config = use('Config')
    const apiVersion = Config.get('app.api_version')
    existingResponse.version = apiVersion
    ctx.response.send(existingResponse)
  }
}

module.exports = ResponseWithVersion
