const Event = use('Event')
const Mail = use('Mail')

Event.on('email::verification', async (params) => {
  await Mail.send(params.template, params.data, (message) => {
    message
      .to(params.to)
      .from(params.from)
      .subject(params.subject)
  })
})

Event.on('email::resetPassword', async (params) => {
  await Mail.send(params.template, params.data, (message) => {
    message
      .to(params.to)
      .from(params.from)
      .subject(params.subject)
  })
})

Event.on('email::completeAccount', async (params) => {
  await Mail.send(params.template, params.data, (message) => {
    message
      .to(params.to)
      .from(params.from)
      .subject(params.subject)
  })
})
